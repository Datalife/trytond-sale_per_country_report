# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .sale import (SaleCountryNote, PrintSaleCountryNote,
   PrintSaleCountryNoteParam, SaleConfiguration,
   ConfigurationReportCountryFilter)


def register():
    Pool.register(
        SaleConfiguration,
        ConfigurationReportCountryFilter,
        PrintSaleCountryNoteParam,
        module='sale_per_country_report', type_='model')
    Pool.register(
        PrintSaleCountryNote,
        module='sale_per_country_report', type_='wizard')
    Pool.register(
        SaleCountryNote,
        module='sale_per_country_report', type_='report')
